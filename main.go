package main

import (
	"encoding/json"
	//"fmt"
)

type Host struct {
	Ip      string
	Aliases []string
}

func Parse(str string) (hosts []Host) {
	strLen := len(str)
	if strLen == 0 {
		return
	}
	var words []string
	var prv, cur int
	inc := 0
	var end int
	bgn := 0
	for bgn < strLen {
		for bgn < strLen && (str[bgn] == ' ' || str[bgn] == '\t' || str[bgn] == '\n') {
			if str[bgn] == '\n' {
				cur = inc
			}
			bgn++
		}
		end = bgn
		for end < strLen && str[end] != ' ' && str[end] != '\t' && str[end] != '\n' && str[end] != '#' {
			end++
		}
		if bgn != end {
			//println(prv, cur, inc, str[bgn:end])
			if inc != 0 && inc == cur {
				//fmt.Println(words[prv:cur])
				if (prv + 1) < cur {
					hosts = append(hosts, Host{words[prv], words[prv+1 : cur]})
				}
				prv = cur
			}
			inc++
			words = append(words, str[bgn:end])
		}
		if str[end] == '#' {
			for end < strLen && str[end] != '\n' {
				end++
			}
		}
		if str[end] == '\n' {
			cur = inc
		}
		bgn = end + 1
	}
	//fmt.Println(words[prv:])
	if len(words[prv:]) > 1 {
		hosts = append(hosts, Host{words[prv], words[prv+1:]})
	}
	return
}
func main() {
	str := `
127.0.0.1  localhost djjdjd hdjdj# djdjjdj
::1  localhost #gdgdh

# [163.com]
127.0.0.1 analytics.163.com 
127.0.0.1  crash.163.com		
127.0.0.1  crashlytics.163.com
127.0.0.1    iad.g.163.com
jdjdjdjd
   127.0.0.1 jdjsjsb.jsjsj
# [1mobile.com]
127.0.0.1 ads.1mobile.com
127.0.0.1 api4.1mobile.com
`
	bs, _ := json.MarshalIndent(Parse(str), "", "  ")
	println(string(bs))

	str = `
# [zapr.in]  
   127.0.0.1 appmm.zapr.in
 127.0.0.1 sdk.zapr.in  #
127.0.0.1 submit.zapr.in#
    #hdjjdj
# [zarget.com]
127.0.0.1 zarget.com jdjdjdj
127.0.0.1 cdn.zarget.com
`
	bs, _ = json.MarshalIndent(Parse(str), "", "  ")
	println(string(bs))
}
